# Primitive Dash G

Proyecto final para el curso de Programación de Videojuegos 3 de la 
Tecnicatura de Diseño y Programación de Videojuegos <FICH-UNL-FRSF>.

Final project for a Videogames programming course.

## Known bugs: 
-Visuar Raster throws an error when returning from a level to the main menu

-Sometimes, an arrow may not load the collision shape, or is not setting the layer,
hard to reproduce and goes away reloading the level. (With Unity I've got a similar problem)

-Level 4 is too hard on Android, maybe a delay on the touch events make it harder.

## Licenses
MIT for code related files (.tscn, .gd, .tres)
CC0 for Blender files and image (png) files.
Titlefont is a resource based on Stix OpenType.

The engine is Godot Engine (MIT) [https://godotengine.org](https://godotengine.org)

Sound Effects by Kenney (CC0) [http://kenney.nl](http://kenney.nl)

Music "Trisect Field" by The One G (CC-BY-SA 4.0) [https://www.myspace.com/the1g](https://www.myspace.com/the1g)


### Other:
Sorry about the camel case but I can't avoid it, and is faster for me.
Comments are in spanish.
