
extends KinematicBody

###
# Control principal para HeroCube, se intenta que el movimiento sea siempre relativo.
# Algunas cosas sólo afectan el mesh para evitar movimientos innecesarios del KinematicBody
###

export (float) var advanceVelocity = 100.0
export (float) var sideVelocity = 100.0
export (float) var gravity = -5.0
var movement = Vector3(0,0,0)
var remainingSide = 0
var sideModifier = 0

var startTransform

func _ready():
	set_fixed_process(true)
	var pos = get_tree().get_root().get_node("main/levelsgohere/level/dropposition").get_translation()
	pos.y = get_translation().y
	set_translation(pos)
	startTransform = get_transform()
	pass

func _fixed_process(delta):	
	
	move(Vector3(0,gravity*delta,0))
	if !get_node("AnimationPlayer").is_playing()&&get_node("AnimationPlayer").get_current_animation()=="deadanim":
		reset()
	elif (is_colliding()):
		if get_node("AnimationPlayer").is_playing()&&get_node("AnimationPlayer").get_current_animation()=="introanim":
			get_node("AnimationPlayer").stop()
			movement.z = -advanceVelocity
			var newPos = get_translation()
			newPos.x = stepify(newPos.x,0.5)
			newPos.z = stepify(newPos.z,0.5)
			move_to(newPos)
			get_node("MeshInstance").set_rotation(Vector3(0,0,0))
		if Input.is_action_just_pressed("ui_left")&&remainingSide<=0:
			remainingSide = 62
			sideModifier = -1
		if Input.is_action_just_pressed("ui_right")&&remainingSide<=0:
			remainingSide = 62
			sideModifier = 1
	
	if sideModifier!=0:
		var sideMove = sideVelocity*delta
		remainingSide-=sideMove
		if remainingSide<=0:
			sideModifier = 0
		movement.x = sideModifier*sideMove
	
	move(Quat(get_transform().basis).xform(movement)*delta)

func playdead():
	get_node("AnimationPlayer").play("deadanim")
	movement.z = 0

func reset():
	get_node("MeshInstance").set_scale(Vector3(1,1,1))
	set_transform(startTransform)
	movement = Vector3(0,0,0)
	movement.x = 0
	movement.z = 0
	sideModifier = 0
	remainingSide=0
	get_node("AnimationPlayer").play("introanim")

func _on_Area_area_enter( area ):

	if area.is_in_group("arrow"):
		get_node("sounds").play("turn")
		var newPos = get_translation()
		newPos.x = stepify(newPos.x,0.5)
		newPos.z = stepify(newPos.z,0.5)
		move_to(newPos)
		movement.x = 0
		sideModifier = 0
		remainingSide=0
		set_transform(Transform(area.get_transform().basis,get_transform().origin))
	elif area.is_in_group("instakill"):
		get_node("sounds").play("dead")
		playdead()
	elif area.is_in_group("goal"):
		get_node("sounds").play("goal")
		get_node("../ingameui/Control").goalReached()
		
