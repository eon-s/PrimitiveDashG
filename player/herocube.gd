
extends RigidBody

###
# Primer intento para el cubo, usando RigidBody
###

onready var startTransform = get_transform()

func _ready():
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	if Input.is_action_pressed("ui_up"):
		set_linear_velocity(Quat(get_transform().basis).xform(Vector3(0,0,-6)))
	if Input.is_action_just_pressed("ui_left"):
		translate(Vector3(-1,0,0))
	if Input.is_action_just_pressed("ui_right"):
		translate(Vector3(1,0,0))
		#global_translate(Quat(get_transform().basis).xform(Vector3(-1,0,0)))
	

func reset():
	set_transform(startTransform)
	set_linear_velocity(Vector3(0,0,0))
	#set_linear_velocity(Quat(get_transform().basis).xform(Vector3(0,0,-6)))

func _on_Area_area_enter( area ):
	
	if area.is_in_group("arrow"):
		set_transform(Transform(area.get_transform().basis,get_transform().origin))
		set_linear_velocity(Quat(get_transform().basis).xform(Vector3(0,0,-6)))
	if area.is_in_group("instakill"):
		reset()
	
