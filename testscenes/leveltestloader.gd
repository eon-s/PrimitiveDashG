extends Spatial

###
# Cargador de niveles para el main de pruebas
###

func _ready():
	loadLevel()
	pass

func loadLevel():
	var chld = ResourceLoader.load("res://testscenes/testlevel.tscn").instance()
	add_child(chld)
