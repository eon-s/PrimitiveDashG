extends Spatial

###
# cargador de niveles usado por levelsgohere, admite correr main.tscn directamente
###

func _ready():
	loadLevel()
	pass

func loadLevel():
	if get_node("/root/globals") != null:
		var sellvl = get_node("/root/globals").get_selected_path()
		var chld
		if sellvl == "":
			chld = ResourceLoader.load("res://testscenes/testlevel.tscn").instance()
		else:
			chld = ResourceLoader.load(sellvl).instance()
		add_child(chld)
	else:
		var chld = ResourceLoader.load("res://testscenes/testlevel.tscn").instance()
		add_child(chld)
	pass
