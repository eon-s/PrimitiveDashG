extends ScrollContainer

###
# Contiene un arreglo de botones que marcan la selección de un nivel
# El scroll por drag no fucnciona bien en móviles
###

var selectedPath = null
var scrollDirection = 0
export(PackedScene) var LevelButton

func _ready():
	loadLevelButtons()
	set_process_input(true)
	pass

func _process(delta):
	set_h_scroll(get_h_scroll()+500*delta*scrollDirection)

func _input(event):
	if (selectedPath != null && event.type != InputEvent.SCREEN_DRAG):
		get_node("/root/globals").set_selected_path(selectedPath)
		get_tree().change_scene("res://game/main.tscn")
	elif event.type == InputEvent.SCREEN_DRAG:
		selectedPath = null

func loadLevelButtons():
	var lvls = get_node("/root/globals").get_levels()
	
	for l in lvls:
		var lb = LevelButton.instance()
		var name = l.split(".t")[0]
		
		name = name.split("levels/")[1]
		lb.setLevelName(name)
		lb.setLevelPath(l)
		lb.setEnabled(true)
		get_node("HBoxContainer").add_child(lb)
		lb.connect("pressed", self, "_on_levelbutton_pressed", [lb,l])
		lb.connect("released", self, "_on_levelbutton_released", [lb])
	get_node("HBoxContainer").queue_sort() #En Linux, al menos, se cargan desordenados

func _on_levelselect_pressed():
	get_parent().set_hidden(false)

func _on_levelbutton_pressed(btn,path):
	btn.setPressed()
	selectedPath = path

func _on_levelbutton_released(btn):
	btn.setReleased()
	selectedPath = null


func _on_ScrollContainer_visibility_changed():
	if !is_visible():
		set_process(false)

func _on_scrollleft_button_down():
	scrollDirection = -1
	set_process(true)

func _on_scrollleft_button_up():
	scrollDirection = 0
	set_process(false)

func _on_scrollright_button_down():
	scrollDirection = 1
	set_process(true)

func _on_scrollright_button_up():
	scrollDirection = 0
	set_process(false)
