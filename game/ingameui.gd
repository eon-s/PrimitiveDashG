extends Control

###
# Controla animaciones y botones del juego
###

func _ready():
	get_node("AnimationPlayer").play("resetvalues")

func goalReached():
	get_tree().set_pause(true)
	get_node("AnimationPlayer").play("goalreached")

func _on_pause_toggled( pressed ):
	get_tree().set_pause(pressed)

func _on_back_pressed():
	get_tree().set_pause(false)
	get_tree().change_scene("res://game/menu.tscn")

func _on_restart_pressed():
	get_node("AnimationPlayer").play("resetvalues")
	get_tree().set_pause(false)
	get_tree().call_group(0,"player","reset")
