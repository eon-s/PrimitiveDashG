extends Node

###
# Clase que carga niveles, no consigo hacerla funcionar para archivos empaquetados
###

var selectedLevel = -1 setget set_selected_level, get_selected_level
var selectedPath = "" setget set_selected_level, get_selected_level
var levels = Array() setget , get_levels

var baseLevelDir = "res://levels"
var userLevelDir = "user://levels"

func _ready():
	pass

func _init():
	loadLevels(baseLevelDir)
		
	if checkUserDir():
		loadLevels(userLevelDir)
	

func loadLevels(var dirPath):
	var dir = Directory.new()
	
	dir.open(dirPath)
	dir.list_dir_begin()
	var file_name = dir.get_next()
	
	while(file_name!=""): 
		if dir.current_is_dir():
			pass
		else:
			levels.push_back(dirPath+"/"+file_name)
		file_name = dir.get_next()
	levels.sort()
	print(levels)

func checkUserDir():
	var dir = Directory.new()
	dir.open(userLevelDir)
	
	return dir.dir_exists(userLevelDir)
	

func set_selected_level(l):
	selectedLevel = l

func set_selected_path(l):
	selectedPath = l

func get_selected_path():
	return selectedPath

func get_selected_level():
	return selectedLevel

func get_levels():
	return levels