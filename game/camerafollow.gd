
extends Position3D

###
# Intento de cámara que persigue al jugador, manejada desde un nodo que persigue a "target"
###

export(NodePath) var targetPath
onready var target = get_node(targetPath)
export (float) var distance = 10.0
export (float) var height = 5.0
export (float) var angleSide = 20.0
var value = 0

func _ready():
	set_fixed_process(true)
	var newtr = target.get_translation()
	if (newtr.y>2): newtr.y=2
	set_translation(newtr)
	var xside = tan(deg2rad(angleSide))*distance
	get_node("Camera").look_at_from_pos(newtr+Vector3(xside,height,distance),self.get_translation(),Vector3(0,1,0))


func _fixed_process(delta):
	set_transform(Quat(get_transform().basis).slerp(target.get_transform().basis,5*delta))
	
	var newtr = target.get_translation()
	set_translation(newtr)
	
